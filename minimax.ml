open List

module type Game = sig
  type state
  type action 
  val possible_moves : state -> action list
  val move : state -> action -> bool -> state
  val value : state -> int 
  val finish : state -> bool 
end ;;

module Minimax(M : Game) = struct
  let best_action state turn = 
    let rec minimax si ti = 
      let auxiliar_function operation player_turn = fun acc e ->  
        let current_value,current_action = acc in 
        let value,_ = minimax (M.move si e player_turn) (not player_turn) in
        if operation value current_value then value,Some e else acc in 
      match M.finish si with 
      | true -> (M.value si,None)
      | false -> begin 
          let moves = M.possible_moves si in 
          match ti with 
          | true  -> fold_left (auxiliar_function (>)  (true)) (min_int,None) moves
          | false -> fold_left (auxiliar_function (<) (false)) (max_int,None) moves
        end
    in let _,action = minimax state turn in action
end ;;
