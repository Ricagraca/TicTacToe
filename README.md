# Tic Tac Toe

## Introduction
A little game of <b>Tic TacToe</b> written in Objective Caml using the minimax 
algorithm so that the player can never win. Uses a graphical window so that the
user can use only his mouse. You need to have ocaml with the graphics library
installed and that's it. This project uses a simple functor in the file minimax.ml
where it returns the function that will evaluate the state of the TicTacToe board
and calculate the best action. This algorithm can only be used in small games, but
using the alfa beta pruning is possible to extend to bigger games, like chess, etc.

## Compile
Windows:

    make.bat

Linux:

    make
    
## Execute

Windows:

    tictactoe.exe 

Linux:

    ./tictactoe

## Contact
Author: Graça, Ricardo (ricagraca.student@gmail.com)