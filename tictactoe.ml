open Minimax;;
open Array;;
open Graphics;;

exception Draw;;
exception WrongPosition;;

let window_size = 450

let create () = init 9 (fun _ -> 0)

let possible_moves state = 
  let len = length state in 
  let rec getList i = 
    match i < len with 
    | true  -> let seg = (getList (i+1)) in if state.(i) = 0 then i :: seg else seg
    | false -> []
  in getList 0

let move state action turn = 
  let copied_board = Array.copy state in 
  match copied_board.(action) != 0 with 
  | true -> raise (WrongPosition)
  | false ->
    copied_board.(action) <- if turn then 1 else -1;
    copied_board

let value state = 
  let equal a b c = if a != 0 && a = b && b = c then a else 0 in 
  let (or) = fun a b -> if a != 0 then a else (if b != 0 then b else 0) in 
  match state with 
  |[|a;b;c;d;e;f;g;h;i|] ->
    (equal a b c) or (equal d e f) or (equal g h i) or 
    (equal a d g) or (equal b e h) or (equal c f i) or 
    (equal a e i) or (equal c e g) 
  | _ -> 0

let finish state = 
  match value state  with 
  | -1 -> true 
  |  1 -> true 
  |  _ -> begin 
      match possible_moves state with 
      |[] -> true 
      | _ -> false
    end

module TicTacToe = Minimax(struct
    type state = int array
    type action = int
    let possible_moves = possible_moves
    let move = move
    let value = value
    let finish = finish
  end);;

let print_cross = 
  let color = green in 
  fun x y -> 
    set_color color;
    moveto (0) (0);
    let dx = 25 in
    let (a,b,c,d) = (x+dx,y-dx,x-dx+window_size/3,y+dx-window_size/3) in 
    let (e,f,g,h) = (x-dx+window_size/3,y-dx,x+dx,y+dx-window_size/3) in 
    draw_segments [|(a,b,c,d);(e,f,g,h)|];
    set_color black

let print_circle = 
  let color = red in 
  fun x y -> 
    set_color color;
    moveto (0) (0);
    draw_circle (x + window_size/6) (y + window_size/6) (window_size/8);
    set_color black

let draw_lines () = 
  let (a,b,c,d) = (150,0,150,450) in 
  let (e,f,g,h) = (300,0,300,450) in 
  let (i,j,k,l) = (0,150,450,150) in 
  let (m,n,o,p) = (0,300,450,300) in 
  draw_segments [|(a,b,c,d);(e,f,g,h);(i,j,k,l);(m,n,o,p)|]

let textit text = 
  draw_string text;
  let _ = wait_next_event [Button_down] in
  clear_graph ();
  set_color black;
  draw_lines ();
  create () 

let read_position () = 
  let _ = wait_next_event [Button_down] in
  let (a,b) = mouse_pos () in
  let p = 3*(2-(3*b/window_size))+(3*a/window_size) in 
  (a,b,p)

let rec interactive state =
  match finish state with 
  | true -> let new_state = 
              match value state with 
              | -1 -> textit "LOST"
              |  1 -> textit "WIN!" (*WILL NEVER HAPPEN*)
              |  _ -> textit "DRAW" in 
    interactive (new_state)
  | false -> let (a,b,p) = read_position () in 
    let myboard = 
      try move state (p) true with WrongPosition -> state in 
    match myboard == state with 
    | true  -> interactive state 
    | false -> let action = TicTacToe.best_action myboard false in    
      let pa = ((window_size/3)*(3*a/window_size)) in
      let pb = (window_size/3 + (window_size/3)*(3*b/window_size)) in 
      print_cross (pa) (pb);
      match action with 
      | None       -> interactive (textit "DRAW")
      | Some value -> let (c,d) = value mod 3, value / 3 in 
        print_circle ((c)*(window_size/3)) ((2-d)*(window_size/3));
        let opponent_board = (move myboard value false)
        in interactive (opponent_board)

let _ = 
  let size_of_window = string_of_int window_size in 
  open_graph (" "^size_of_window^"x"^size_of_window);
  set_window_title "Tic Tac Toe";
  set_line_width 5;
  draw_lines ();
  try 
    interactive (create ());
  with 
    Draw -> () | Graphics.Graphic_failure m -> ();
